import {useState, useEffect, useContext} from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Component } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';
import home from './logos/home-icon.png';
import './Components.css';



export default function AppNavbar() {

	const { user } = useContext(UserContext);
	const size = {
		height: 24,
		width: 24,
	}
	const attribute = "Icon made by Pixel perfect from www.flaticon.com"
	

	return(
		<Navbar fixed="top" bg="light" expand="lg">
			<Navbar.Brand className="brandName">
				<Link to="/" >
					<img src = {home} style={size} /> 
				</Link> Kentomi Organics
			</Navbar.Brand>

			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav.Link as={NavLink} to="/" >Home</Nav.Link>
				
				
             	<Nav.Link as={NavLink} to="/products" >Products</Nav.Link>
				<Nav.Link as={NavLink} to="/aboutus">About Us</Nav.Link>
				<Nav.Link as={NavLink} to="/contactus">Contact Us</Nav.Link>

				
				{ (user.id) ?
					<>
						<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
						<Nav.Link as={NavLink} to="/partnership" >Partnership</Nav.Link>
					</>
						
				:
					<>
						<Nav.Link as={NavLink} to="/login" >Login</Nav.Link>
						<Nav.Link as={NavLink} to="/register" >Register</Nav.Link>
								
					</>
				}

				{ (user.isAdmin) ?
					<Nav.Link as={NavLink} to="/dashboard">Admin Dashboard</Nav.Link>
				:
					<></>
				}
								

			</Navbar.Collapse>
		</Navbar>
	);
}