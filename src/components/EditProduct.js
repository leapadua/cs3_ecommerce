import React, {useRef, useEffect, useState, useCallback} from 'react';
import { Form, Button } from 'react-bootstrap';
import {Link, Redirect, useHistory} from 'react-router-dom';
import styled from 'styled-components';
import Swal from 'sweetalert2';
import { MdClose } from 'react-icons/md';



const Background = styled.div`
	width: 100%;
	height: 100%;
	background : rgba(0,0,0,0.8);
	position: fixed;
	display: flex;
	justify-content: center;
	align-items: center;
	z-index: 1;
`;

const ModalWrapper = styled.div`
	width: 800px;
	height: 500px;
	box-shadow: 0 5px 16 px rgba(0,0,0,0.2);
	background: #fff;
	color: #000;
	display: absolute;
	position: fixed;
	z-index: 10;
	border-radius: 10px;
`;

const ModalContent = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	line-height: 1.8;
	color: #141414;
	margin: 25px;
	padding: 5px;

	h1{
		align-items: right;
		margin-bottom: 1rem;
	}
`;

const CloseModalButton = styled(MdClose)`
	cursor: pointer;
	position: absolute;
	top: 20px;
	right: 20px;
	width: 32px;
	height: 32px;
	padding: 0;
	z-index: 10;
`;


export default function EditProduct ({showModal, setShowModal}) {
	const history = useHistory();
	const modalRef = useRef();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(false);

	const closeModal = e => {
		if(modalRef.current === e.target){
			setShowModal(false);
		}
	};

	const keyPress = useCallback(e => {
		if(e.key == 'Escape' && showModal) {
			setShowModal(false);
		}
	}, [setShowModal, showModal]);

	function saveProduct(event){
		event.preventDefault();

		fetch('https://afternoon-savannah-89326.herokuapp.com/products/add', {
    		method: "POST",
    		headers: {
    			'Content-Type': 'application/json',
    			'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
    		},
    		body: JSON.stringify({
    			name: name,
    			description: description,
    			price: price
    		})
    	})
    	.then(res => res.json())
		.then(data => {
			// alert('Registered successfully!');
		    Swal.fire(
		    {
		    	title: `Changes on ${data.name} is successfully!`,
		    	icon: `success`
		    })
		    history.push('dashboard');
		})
	};

	useEffect(() => {
		document.addEventListener('keydown', keyPress);
		return () => document.removeEventListener('keydown', keyPress);
	}, [keyPress]);

	useEffect(() => {
    	if(name !== '' && description !== '' && price !== '') {
    		setIsActive(true)
    	} else {
    		setIsActive(false);
    	}
    }, [name, description, price]);

	return(showModal) ? (
		<Background ref={modalRef} onClick={closeModal}>
			<ModalWrapper showModal={showModal}>
				<ModalContent>
					<h1>Add New Product</h1>

					<Form onSubmit={e => saveProduct(e)}>
						<Form.Group controlId="productName">
							<Form.Label>Name: </Form.Label>
							<Form.Control type="text" placeholder="Product Name"
								value={name} onChange={event => setName(event.target.value)} required />
						</Form.Group>

						<Form.Group controlId="productDesc">
							<Form.Label>Description: </Form.Label>
							<Form.Control type="text" placeholder="Product Description"
								value={description} onChange={event => setDescription(event.target.value)} required />
						</Form.Group>

						<Form.Group controlId="price">
							<Form.Label>Price: </Form.Label>
							<Form.Control type="text" placeholder="PHP"
								value={price} onChange={event => setPrice(event.target.value)} required />
						</Form.Group>

						{/*Button*/}
						  { isActive ?
						  		<Button type="submit" id="submitBtn" variant="success" className="btn btn-block">Save Changes</Button>
						  	:
						  		<Button type="submit" id="submitBtn" variant="warning" className="btn btn-block" disabled>Save Changes</Button>
						  }
					</Form>
					
				</ModalContent>
				<CloseModalButton aria-label='Close modal' onClick={() => setShowModal(prev => !prev)} />
			</ModalWrapper>
		</Background>
	) : null;
}



