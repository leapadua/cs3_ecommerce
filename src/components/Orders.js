import React, {useRef, useEffect, useState, useCallback, useContext} from 'react';
import { Form, Button, Container, Table } from 'react-bootstrap';
// import {Link, Redirect, useHistory} from 'react-router-dom';
import styled from 'styled-components';
import Swal from 'sweetalert2';
import { MdClose } from 'react-icons/md';
import AllOrders from '../components/AllOrders';
import UserContext from '../UserContext';



const Background = styled.div`
	width: 100%;
	height: 100%;
	background : rgba(0,0,0,0.8);
	position: fixed;
	display: flex;
	justify-content: center;
	align-items: center;
	z-index: 1;
`;

const ModalWrapper = styled.div`
	width: 1100px;
	height: 500px;
	box-shadow: 0 5px 16 px rgba(0,0,0,0.2);
	background: #fff;
	color: #000;
	display: absolute;
	position: fixed;
	z-index: 10;
	border-radius: 10px;
`;

const ModalContent = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	line-height: 1.8;
	color: #141414;
	margin: 25px;
	padding: 5px;

	h1{
		align-items: right;
		margin-bottom: 1rem;
	}
`;

const CloseModalButton = styled(MdClose)`
	cursor: pointer;
	position: absolute;
	top: 20px;
	right: 20px;
	width: 32px;
	height: 32px;
	padding: 0;
	z-index: 10;
`;



export default function Orders ({showOrdersModal, setShowOrdersModal}) {
	// const history = useHistory();
	const modalRef = useRef();
	const [allOrders, setAllOrders] = useState([]);
	const { user } = useContext(UserContext);
	const scroll = {
		overflow: 'scroll',
		width: '100%'
	}

	const closeModal = e => {
		if(modalRef.current === e.target){
			setShowOrdersModal(false);
		}
	};

	const keyPress = useCallback(e => {
		if(e.key == 'Escape' && showOrdersModal) {
			setShowOrdersModal(false);
		}
	}, [showOrdersModal, setShowOrdersModal]);

	// function getAllOrders(event){

	useEffect(() => {
	fetch('https://afternoon-savannah-89326.herokuapp.com/products/orders', {
    	headers: {
    		'Content-Type': 'application/json',
   			'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
   		}
	})
		.then(response => response.json())
		.then(convertedData => {
			setAllOrders(Object.entries(convertedData).map(orders => {
				console.log(orders);
				return(
					<AllOrders orderInfo={ orders } />
				);
			}));
		});
	});


	useEffect(() => {
		document.addEventListener('keydown', keyPress);
		return () => document.removeEventListener('keydown', keyPress);
	}, [keyPress]);

	// useEffect(() => {
 //    	if(name !== '' && description !== '' && price !== '') {
 //    		setIsActive(true)
 //    	} else {
 //    		setIsActive(false);
 //    	}
 //    }, [name, description, price]);

	return(showOrdersModal) ? (
		<Background ref={modalRef} onClick={closeModal}>
			<ModalWrapper showModal={showOrdersModal} style={{overflow:'scroll'}}>
				<ModalContent>
					<h1>All Orders</h1>
					
					<Container className="row m-5" >
						<Table striped bordered hover style={{ width: '100%' }}>
						  <thead>
						    <tr>
						      <th style={{ width: '40%' }}>Product Name</th>
						      <th style={{ width: '10%' }}>Price</th>
						      <th style={{ width: '50%' }}>Orders</th>
						    </tr>
						  </thead>
						  <tbody>
						    {allOrders}
						  </tbody>
						</Table>
					</Container>

				</ModalContent>
				<CloseModalButton aria-label='Close modal' onClick={() => setShowOrdersModal(prev => !prev)} />
			</ModalWrapper>
		</Background>
	) : null;
}



