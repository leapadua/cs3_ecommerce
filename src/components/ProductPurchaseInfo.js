import React, { useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { Link, NavLink, useHistory } from 'react-router-dom';
import styled from 'styled-components';
// import EditProductModal from '../components/EditProduct';   // Modal



export default function ProductPurchaseInfo({prodInfo}) {

	const history = useHistory();
	const { _id, name, description, price } = prodInfo;
	const [showModal, setShowModal ] = useState(false);

	const margin = {
		margin: "2px"
	}

	const viewProduct = () => {
		history.push(`/products/${_id}`);
	}

	return(
		<>
		{/*<EditProductModal showModal={showModal} setShowModal={setShowModal} />*/}
		<tr>
		  <td>{ name }</td>
		  <td>{ description }</td>
		  <td>{ price }</td>
		  <td  style={{ textAlign:"center" }}>
			<tr>
		  		{<Button style={margin} size="sm" variant="outline-success" onClick={viewProduct} >Details</Button>}
		  		{/*<Link className="btn outline-success" style={margin} size="sm" to={`/courses/${_id}`}> See Details </Link>*/}
		  	</tr>
		  </td>
		</tr>
		</>
	);
}