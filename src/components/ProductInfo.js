import React, { useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import styled from 'styled-components';
// import EditProductModal from '../components/EditProduct';   // Modal



export default function ProductInfo({prodInfo}) {

	const { _id, name, description, price } = prodInfo;
	const [showModal, setShowModal ] = useState(false);

	const margin = {
		margin: "2px"
	}

	const editProduct = () => {
		setShowModal(prev => !prev);
	}

	return(
		<>
		{/*<EditProductModal showModal={showModal} setShowModal={setShowModal} />*/}
		<tr>
		  <td>{ name }</td>
		  <td>{ description }</td>
		  <td>{ price }</td>
		  <td  style={{ textAlign:"center" }}>
			<tr>
		  		<Button style={margin} size="sm" variant="outline-success" onClick={editProduct}>Update</Button>
		  	</tr>
		  	<tr>
		  		<Button size="sm" variant="outline-danger">Disable</Button>
			</tr>
		  </td>
		</tr>
		</>
	);
}