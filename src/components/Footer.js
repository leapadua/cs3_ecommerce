import './Components.css';
import facebook from './logos/facebook.png';
import instagram from './logos/instagram.png';
import youtube from './logos/youtube.png';
import pinterest from './logos/pinterest.png';

export default function Footer() {

	return(
		<div className="main-footer">

			<div className="container">

				{/*Shop Logo*/}
				<div className="row">
					{/*Place logo here*/}
				</div>

				<div className="row">
					{/*Left Panel -------------------------------*/}
					<div className="col-4">
						{/*Other Links*/}
						<div className="row">
							<div className="col">
								<h5>OTHER LINKS</h5>
								<ul className="list-unstyled">
									<li>Science</li>
									<li>Usana Health Supplements</li>
									<li>Healthy Lifestyle</li>
								</ul>
							</div>
						</div>	
					</div>
					{/*Center Panel -------------------------------*/}
					<div className="col-4">
						{/*Company*/}
						<div className="row">
							<div className="col">
								<h5>COMPANY</h5>
								<ul className="list-unstyled">
									<li>Home</li>
									<li>Products</li>
									<li>About Kentomi Organics</li>
									<li>Partnership</li>
								</ul>
							</div>
						</div>
					</div>
					{/*Right Panel -------------------------------*/}
					<div className="col-4">
						{/*Connect*/}
						<div className="row">
							<div className="col">
								<h5>CONNECT WITH US</h5>
								<ul className="list-unstyled">
									<li>Contact Us</li>
									<li>Kentomi Organics Login</li>
								</ul>
							</div>
						</div>
						{/*Social Media Platforms*/}
						<div className="row">
							<div className="col">
								<h5>SOCIAL MEDIA</h5>
								<span className="list-unstyled">
									<img src = {facebook} className="logo" /> &nbsp;&nbsp;
									<img src = {instagram} className="logo" /> &nbsp;&nbsp;
									<img src = {youtube} className="logo" /> &nbsp;&nbsp;
									<img src = {pinterest} className="logo" /> &nbsp;&nbsp;
								</span>
							</div>
						</div>
					</div>
				</div>
				<hr />

				<div className="row">
					<p className="col-sm text-center">
						&copy;{new Date().getFullYear()} Kentomi Organics | All Rights Reserved | Terms of Service | Privacy 
					</p>
				</div>

			</div>
			
		</div>
	);
}