import React from 'react';
import { Row, Col } from 'react-bootstrap';



export default function Banner({data}) {

	const {title,tagline} = data;

	const customize = {
		background: "rgba(242,230,217,0.5)",
		margin: "80px 0px 0px 0px",
		width: "100%"
	}

	return(
		<Row style={customize}>
			<Col  className="p-5">
				<h1>{title}</h1>
				<p>{tagline}</p>
			</Col>
		</Row>
	);
}