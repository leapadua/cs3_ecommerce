import React, { useEffect, useState } from 'react';
import { Card, Table, Button } from 'react-bootstrap';
import styled from 'styled-components';
// import OrderItems from '../components/OrderItems';
// import EditProductModal from '../components/EditProduct';   // Modal



export default function AllOrders({orderInfo}) {

	// const { name, price } = orderInfo;
	const [showModal, setShowModal ] = useState(false);
	console.log(orderInfo);
	const orderList = orderInfo[1].orders;
	console.log(orderList);

	const editProduct = () => {
		setShowModal(prev => !prev);
	}

	return(
		<>
			{/*<EditProductModal showModal={showModal} setShowModal={setShowModal} />*/}
			<tr>
				<td>{ orderInfo[1].name }</td>
				<td>{ orderInfo[1].price }</td>
				<td>
					{
						orderList.map((orders,index) => (
							<>
								<tr data-index={index}>
									<td>{orders.orderId}</td>
									<td>{orders.purchasedOn}</td>
									<td>{orders.quantity}</td>
								</tr>
							</>
						))
					}		  
				</td>
			</tr>
		</>
	);
}