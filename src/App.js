import React, { Fragment, useState, useEffect } from 'react';
import { Route, Switch, BrowserRouter as Router }from 'react-router-dom'; 
import { UserProvider } from './UserContext';
import Navbar from './components/AppNavbar';
import Footer from './components/Footer';
import Landpage from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Products from './pages/Catalog';
import AdminDashboard from './pages/AdminDashboard';
import ProductView from './pages/ProductView';
import Error from './pages/Error';
import './App.css';


export default function App() {

  const [user, setUser] = useState({
    id : null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
      isAdmin: null
    });
  }

  useEffect(() => {
    console.log();
    fetch('https://afternoon-savannah-89326.herokuapp.com/users/details', { 
      // method: "POST",
      headers: {
        'Content-Type' : 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null
        });
      }
    })
  });

  return (
    <>
      <UserProvider value={{user, unsetUser, setUser}}>
        <div className="page-container">
          <div className="content-wrap">
            <Router>
              <Navbar />

              <Switch>
                <Route exact path="/" component={ Landpage } />
                <Route exact path="/login" component={ Login } />
                <Route exact path="/register" component={ Register } />
                <Route exact path="/products" component={ Products } />
                <Route exact path="/products/:productId" component={ ProductView } />
                <Route exact path="/dashboard" component={ AdminDashboard } />
                <Route exact path="/logout" component={ Logout } />
                <Route component={Error} />
              </Switch>

           </Router>
          </div>
          <Footer />
        </div>
      </UserProvider>
    </>
  );
}

