import { Form, Button, Container } from 'react-bootstrap';
import {Link, Redirect, useHistory} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import Hero from '../components/Banner';
import UserContext from '../UserContext';



const bannerInfo = {
	title: 'Login Page',
	tagline: 'Access your account and start shopping!'
}


export default function Login(){

	const history = useHistory();
	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	const authenticate = (e) => {
		e.preventDefault();

		fetch('https://afternoon-savannah-89326.herokuapp.com/users/login', { 
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {
			if (typeof data.accessToken !== "undefined"){
				localStorage.setItem('accessToken', data.accessToken);
				retrieveUserDetails(data.accessToken);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to the App!"
				});
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check login details and try again!"
				});
			}
		})
	}

	const retrieveUserDetails = (token) => {

		fetch('https://afternoon-savannah-89326.herokuapp.com/users/details', {
			headers: {
				'Content-Type': 'application/json',
    			'Authorization': `Bearer ${token}`
			}})
		.then(result => result.json())
		.then(convertedResult => {
			console.log(convertedResult);
			setUser({
				id: convertedResult._id,
				isAdmin: convertedResult.isAdmin
			});
		})
	}

	useEffect(() => {
    	if(email !== '' && password !== '') {
    		setIsActive(true)
    	} else {
    		setIsActive(false);
    	}
    }, [email, password]);

	return(
		(user.id) ? 
			<Redirect to="/" />  
		:
		<Container>
			{/*Banner */}
			<Hero data={bannerInfo}/>

			{/*Login */}
			<Form className="mb-5" onSubmit={e => authenticate(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Addres: </Form.Label>
					<Form.Control type="email" placeholder="sample_Email123@mail.com" 
						value={email} onChange={event => setEmail(event.target.value)} required />
				</Form.Group>
				<Form.Group controlId="userPassword" className="mb-2">
					<Form.Label>Password: </Form.Label>
					<Form.Control type="password" placeholder="Password" 
						value={password} onChange={event => setPassword(event.target.value)} required />
				</Form.Group>

				{/*Button*/}
				  { isActive ?
				  		<Button type="submit" id="submitBtn" variant="success" className="btn btn-block">Login</Button>
				  	:
				  		<Button type="submit" id="submitBtn" variant="warning" className="btn btn-block" disabled>Login</Button>
				  }
				<p className="mt-2 text-center">Not yet registered? Sign-up <Link to="/register">here</Link></p>
			</Form>
		</Container>
	);
}
