import React, { useContext, useEffect, useState } from 'react';
import { Form, Button, Container, Table } from 'react-bootstrap';
import { Link, Redirect }from 'react-router-dom'; 
import styled from 'styled-components';
import Hero from '../components/Banner';
import ProductInfo from '../components/ProductInfo';
import AddNewProductModal from '../components/AddNewProduct';   // Modal
import ShowOrdersModal from '../components/Orders';   // Modal
import { GlobalStyle } from '../globalStyles.js';
import UserContext from '../UserContext';



let bannerInfo = {
	title: 'Admin Dashboard',
	tagline: 'This page is restricted to users with administor priviledges!'
}

const Background = styled.div`
	width: 100%;
	height: 100%;
	background : rgba(0,0,0,0.8);
	position: fixed;
	display: flex;
`;

export default function Products() {

	const [product, setProduct] = useState([]);
	const [showModal, setShowModal ] = useState(false);
	const [showOrdersModal, setShowOrdersModal ] = useState(false);
	const { user } = useContext(UserContext);

	useEffect(() => {
		fetch('https://afternoon-savannah-89326.herokuapp.com/products/all')
			.then(response => response.json())
			.then(convertedData => {
				console.log(convertedData);
				setProduct(convertedData.map(product => {
					return(
						<ProductInfo key={product._id} prodInfo={ product } />
					);
				}));
				// setProduct(convertedData[])
			});
	});


	const openEditModal = () => {
		setShowModal(prev => !prev);
	}

	const openOrdersModal = () => {
		setShowOrdersModal(prev => !prev);
	}

	return( (user.isAdmin) ?
		<>
			<AddNewProductModal showModal={showModal} setShowModal={setShowModal} />
			<ShowOrdersModal showOrdersModal={showOrdersModal} setShowOrdersModal={setShowOrdersModal} />
			<Container>
				{/*Banner */}
				<div className='row text-left mb-3' >
					<Hero data={bannerInfo}/>
				</div>

				{/*Dashboard Buttons*/}
				<div className='row mb-5'>
					<div className="col text-right">
						<Button type="button" id="addProduct"  onClick={openEditModal} className="btn btn-lg btn-primary">Add New Product</Button>
					</div>
					<div className="col text-left">
						<Button type="button" id="userOrders" onClick={openOrdersModal} className="btn btn-lg btn-primary">Show User Orders</Button>
					</div>
						
				</div>

				<Container className="row mb-5">
					<Table striped bordered hover style={{ width: '100%' }}>
					  <thead>
					    <tr>
					      <th style={{ width: '30%' }}>Product Name</th>
					      <th style={{ width: '50%' }}>Product Description</th>
					      <th style={{ width: '10%' }}>Price</th>
					      <th style={{ width: '10%' }}>Actions</th>
					    </tr>
					  </thead>
					  <tbody>
					    {product}
					  </tbody>
					</Table>
				</Container>
				
				<GlobalStyle />	
		  	</Container>
	  	</>
	  	:
	  		<Redirect to="/error" />
	);
}

