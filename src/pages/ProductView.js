import { Row, Col, Form, Button, Container, Card } from 'react-bootstrap';
import { useHistory, Redirect } from 'react-router-dom';
import React, {useState, useEffect, useContext} from 'react';
import { Link } from 'react-router-dom';



 export default function CourseView() {

 	const queryParams = (window.location.href).toString().split("/");
 	const parsedUrlId= queryParams[4];
 	console.log(parsedUrlId);
 	const [prodInfo, setProdInfo] = useState('');

	useEffect(() => {
		fetch(`https://afternoon-savannah-89326.herokuapp.com/products/${parsedUrlId}/find`)
			.then(response => response.json())
			.then(convertedData => {
				console.log(convertedData);
				setProdInfo({
					id: convertedData._id,
					name: convertedData.name,
					description: convertedData.description,
					price: convertedData.price
				});
		});
	});

        return (
        	 <Container className="m-5">
				<Row>
					<Col lg={{ span: 6, offset: 3 }}>
						<Card>
							<Card.Body className="text-center">
								<Card.Title>{prodInfo.name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{prodInfo.description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>PhP{prodInfo.price}</Card.Text>
								<Button variant="primary" block>Add to Cart</Button>			
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
        );
}
