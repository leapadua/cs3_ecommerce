import Container from 'react-bootstrap/Container';
import Hero from '../components/Banner';

let bannerInfo = {
	title: "Welcome to Kentomi Organics!",
	tagline: "Where you shop with healthier options!"
}


export default function Home () {
	return(
		<Container >
			<Hero data={bannerInfo} />
		</Container>
	);
}