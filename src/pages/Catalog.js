import {useEffect, useState} from 'react';
import { Form, Button, Container, Table } from 'react-bootstrap';
import { GlobalStyle } from '../globalStyles.js';
import styled from 'styled-components';
import Hero from '../components/Banner';
import ProductPurchase from '../components/ProductPurchaseInfo';
import UserContext from '../UserContext';


let bannerInfo = {
	title: 'Product Catalog Page!',
	tagline: 'Click the "Add To Cart" button to purchase!'
}

export default function Catalog() {

	const [product, setProduct] = useState([]);

	useEffect(() => {
		fetch('https://afternoon-savannah-89326.herokuapp.com/products/active')
			.then(response => response.json())
			.then(convertedData => {
				console.log(convertedData);
				setProduct(convertedData.map(product => {
					return(
						<ProductPurchase key={product._id} prodInfo={ product } />
					);
				}));
			});
	});

	return(
		<Container mb>
			{/*Banner */}
			<div className='row text-left'>
				<Hero data={bannerInfo}/>
			</div>

			<Container className="row mb-5">
				<Table striped bordered hover style={{ width: '100%' }}>
				  <thead>
				    <tr>
				      <th style={{ width: '30%' }}>Product Name</th>
				      <th style={{ width: '50%' }}>Product Description</th>
				      <th style={{ width: '10%' }}>Price</th>
				      <th style={{ width: '10%' }}>Actions</th>
				    </tr>
				  </thead>
				  <tbody>
				    {product}
				  </tbody>
				</Table>
			</Container>
				
			<GlobalStyle />	
		</Container>
	 //  <>
		// <Hero data={bannerInfo}/>
		// {product}
	 //  </>
	);
}

