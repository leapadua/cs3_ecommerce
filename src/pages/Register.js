import { useState, useEffect } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';
import {Link, Redirect, useHistory} from 'react-router-dom';



let forBanner = {
	title: "Register Page!",
	tagline: "You are one step away from creating a new account. Register Now!"
}


export default function Register(){

    const history = useHistory();
    const [ firstName, setFirstName ] = useState('');
    const [ middleInitial, setMiddleInitial ] = useState('');
    const [ lastName, setLastName ] = useState('');
    const [ gender, setGender] = useState('Male');
    const [ email, setEmail ] = useState('');
    const [ password1, setPassword1 ] = useState('');
    const [ password2, setPassword2 ] = useState('');
    const [ mobileNo, setMobileNo ] = useState(''); 

    const [ isRegisterBtnActive, setRegisterBtnActive ] = useState('');
    const [ isComplete, setIsComplete ] = useState(false); 
    const [ isMatched, setIsMatched ] = useState(false);

    function registerUser(event){
    	event.preventDefault();

    	// 'https://ancient-inlet-84848.herokuapp.com/users/register'
    	// https://desolate-shore-72481.herokuapp.com
    	// https://afternoon-savannah-89326.herokuapp.com
    	fetch('https://afternoon-savannah-89326.herokuapp.com/users/register', {
    		method: "POST",
    		headers: {
    			'Content-Type': 'application/json'
    		},
    		body: JSON.stringify({
    			firstName: firstName,
    			lastName: lastName,
    			middleInitial: middleInitial,
    			gender: gender,
    			email: email,
    			password: password1,
    			mobileNo: mobileNo
    		})
    	})
    	.then(res => res.json())
		.then(data => {
			// alert('Registered successfully!');
		    Swal.fire(
		    {
		    	title: `${data.firstName} is registered successfully!`,
		    	icon: `success`,
		    	text: `Enjoy e-shopping!`
		    })
		    history.push('login');
		})
    }


    useEffect(() => {
    	if(password1 === password2 && (password1 !== '' && password2 !== '') ){
    		setIsMatched(true);

    		if (firstName !== '' && middleInitial !== '' && lastName !== '' && email !== '' && 
    			password1 !== '' && password2 !== '' && mobileNo !== '' && mobileNo.length === 11){
    				setRegisterBtnActive(true);
    				setIsComplete(true);
	    	} else {
	    		setRegisterBtnActive(false);
	    		setIsComplete(false);
	    	}
    	} else {
    		setIsMatched(false);
    	}
    }, [firstName, middleInitial, lastName, email, password1, password2, mobileNo]);  

	return(
		<Container>
			{/*Banner */}
			<Hero data={forBanner}/>

			{
             isComplete ? 
               <h5 className="mb-4"> Click the "Create New Account" button to proceed! </h5>
             :
              <h5 className="mb-4"> Please enter your information. </h5>
           	}

			<Form onSubmit={(event) => registerUser(event)} className="mb-5">
				{/*First Name*/}
				<Form.Group controlId="firstName">
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" placeholder="First Name" 
						value={firstName} onChange={event => setFirstName(event.target.value)} required/>
				</Form.Group>

				{/*Middle Name*/}
				<Form.Group controlId="middleName">
					<Form.Label>Middle Initial:</Form.Label>
					<Form.Control type="text" placeholder="Middle Initial" 
						value={middleInitial} onChange={event => setMiddleInitial(event.target.value)} required />
				</Form.Group>

				{/*Last Name*/}
				<Form.Group controlId="lastName">
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" placeholder="Last Name" 
					value={lastName} onChange={event => setLastName(event.target.value)} required />
				</Form.Group>

				{/*Emai Address*/}
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address:</Form.Label>
					<Form.Control type="email" placeholder="sample_Email123@mail.com" 
					value={email} onChange={event => setEmail(event.target.value)} required />
				</Form.Group>

				{/*Password*/}
				<Form.Group controlId="password1">
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Password" 
					value={password1} onChange={event => setPassword1(event.target.value)} required />
				</Form.Group>

				{/*Confirm Password*/}
				<Form.Group controlId="password2">
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" 
					value={password2} onChange={event => setPassword2(event.target.value)} required />
				</Form.Group>

				{
					isMatched ?
						<p className="text-success"> *Passwords matched!* </p>
					:
						<p className="text-danger"> *Passwords should match!* </p>
				}

				{/*Gender*/}
				<Form.Group controlId="gender">
					<Form.Label>Gender:</Form.Label>
					<Form.Control as="select" value={gender} onChange={(e) => setGender(e.target.value)} required>
						<option disabled>Select Gender</option>
						<option value="Male">Male</option>
						<option value="Female">Female</option>
					</Form.Control>
				</Form.Group>

				{/*Mobile Number*/}
				<Form.Group controlId="mobileNo">
					<Form.Label>Mobile Number:</Form.Label>
					<Form.Control type="number" placeholder="Insert Mobile Number Here" 
					value={mobileNo} onChange={event => setMobileNo(event.target.value)} required />
				</Form.Group>

				{/*Create New Account Button*/}
				{/*{registerBtn ? truthy :falsy }*/}
				{ isRegisterBtnActive ? 
					<Button type="submit" id="submitBtn" variant="success" className="btn btn-block">Create New Account</Button>
				:
					<Button type="submit" id="submitBtn" variant="warning" className="btn btn-block" disabled>Create New Account</Button>
				}

				<p className="mt-2 text-center">Already registered? Click <Link to="/register">here</Link> to Login!</p>
			</Form>

		</Container>
	);
}